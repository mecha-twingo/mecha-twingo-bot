FROM node:22

ARG NODE_ARG="development"
ENV NODE_ENV=$NODE_ARG

ENV SCRIPT="serve.js"

RUN curl -fsSL https://get.docker.com | sh

WORKDIR /app
COPY ./commands /app/commands
COPY ./config /app/config
COPY ./entries /app/entries
COPY ./events /app/events
COPY ./src /app/src

COPY ./entrypoint.sh /app/
COPY ./package*.json /app/
COPY ./banner.png /app/banner.png

RUN if [ "$NODE_ENV" != "development" ]; then npm ci --omit=dev; fi

ENTRYPOINT ./entrypoint.sh $SCRIPT