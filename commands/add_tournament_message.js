import { SlashCommandBuilder } from 'discord.js'
import { addTournamentMessage as execute } from '#actions'

export default {
    data: new SlashCommandBuilder()
        .setName('add_tournament_message')
        .setDescription('Ajoute un tournoi dans le canal "agenda-tournois"')
        .addStringOption((option) =>
            option
                .setName('emoji')
                .setDescription('Emoji du tournoi')
                .setRequired(true)
        )
        .addStringOption((option) =>
            option
                .setName('url')
                .setDescription('Lien du tournoi')
                .setRequired(true)
        )
        .addStringOption((option) =>
            option
                .setName('channel_id')
                .setDescription('Identifiant du canal de discussion')
        ),
    execute,
    options: {
        specific: true,
    }
}
