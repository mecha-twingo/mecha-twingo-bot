import { SlashCommandBuilder } from 'discord.js'

export default {
    data: new SlashCommandBuilder()
        .setName('are_you_ok')
        .setDescription('Are you ok ?'),
    execute: async (interaction) => {
        await interaction.reply({ content: 'https://tenor.com/bFVNl.gif', ephemeral: true })
    },
}
