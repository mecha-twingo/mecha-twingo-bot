import 'dotenv/config'

import { SlashCommandBuilder } from 'discord.js'
import { createWeeklies as execute } from '#actions'

export default {
    data: new SlashCommandBuilder()
        .setName('create_weeklies')
        .setDescription('Créé une ou plusieurs weeklies')
        .addStringOption((option) =>
            option
                .setName('weeklies_id')
                .setDescription(
                    'Index des weeklies à créer, mettre " * " pour toutes les créer'
                )
                .setRequired(true)
        ),
    execute,
}
