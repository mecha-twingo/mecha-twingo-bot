import { SlashCommandBuilder } from 'discord.js'
import { deleteTournamentMessage as execute } from '#actions'

export default {
    data: new SlashCommandBuilder()
        .setName('delete_tournament_message')
        .setDescription('Supprime un tournoi du canal "agenda-tournois"')
        .addStringOption((option) =>
            option
                .setName('url')
                .setDescription('Lien du tournoi')
                .setRequired(true)
        ),
    execute,
    options: {
        specific: true,
    }
}
