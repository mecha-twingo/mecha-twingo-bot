import 'dotenv/config'

import { SlashCommandBuilder } from 'discord.js'
import { deleteTournaments as execute } from '#actions'

export default {
    data: new SlashCommandBuilder()
        .setName('delete_tournaments')
        .setDescription('Supprime un ou plusieurs tournois')
        .addStringOption((option) =>
            option
                .setName('tournaments_url')
                .setDescription('URL des tournois à supprimer')
                .setRequired(true)
        ),
    execute,
}
