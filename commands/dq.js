import 'dotenv/config'
import { SlashCommandBuilder } from 'discord.js'
import { disqualifyAttendee as execute } from '#actions'

export default {
    data: new SlashCommandBuilder()
        .setName('dq')
        .setDescription(
            'DQ un joueur de la weekly du canal courant'
        )
        .addUserOption((option) =>
            option
                .setName('participant1')
                .setDescription('Un joueur à dq')
                .setRequired(true)
        )
        .addUserOption((option) =>
            option
                .setName('participant2')
                .setDescription('Un autre joueur à dq')
        )
        .addUserOption((option) =>
            option
                .setName('participant3')
                .setDescription('Un autre joueur à dq')
        )
        .addUserOption((option) =>
            option
                .setName('participant4')
                .setDescription('Un autre joueur à dq')
        )
        .addUserOption((option) =>
            option
                .setName('participant5')
                .setDescription('Un autre joueur à dq')
        )
        .addUserOption((option) =>
            option
                .setName('participant6')
                .setDescription('Un autre joueur à dq')
        )
        .addUserOption((option) =>
            option
                .setName('participant7')
                .setDescription('Un autre joueur à dq')
        )
        .addUserOption((option) =>
            option
                .setName('participant8')
                .setDescription('Un autre joueur à dq')
        )
        .addUserOption((option) =>
            option
                .setName('participant9')
                .setDescription('Un autre joueur à dq')
        ),
    execute
}
