import { SlashCommandBuilder } from 'discord.js'
import { refreshTournamentMessage as execute } from '#actions'

export default {
    data: new SlashCommandBuilder()
        .setName('refresh_tournament_messages')
        .setDescription(
            'Actualise la listes des tournois dans le canal "agenda-tournois" et supprime les précédents'
        ),
    execute,
    options: {
        specific: true,
    }
}
