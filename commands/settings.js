import 'dotenv/config'

import { SlashCommandBuilder } from 'discord.js'
import { settings as execute } from '#actions'

export default {
    data: new SlashCommandBuilder()
        .setName('settings')
        .setDescription('Configure le discord'),
    execute,
}
