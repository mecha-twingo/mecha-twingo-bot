import 'dotenv/config'

import { SlashCommandBuilder } from 'discord.js'
import { postMsgWeeklies as execute } from '#actions'

export default {
    data: new SlashCommandBuilder()
        .setName('show_weeklies')
        .setDescription(
            'Affiche un exemple de message des weeklies dans ce canal'
        )
        .addBooleanOption((option) =>
            option
                .setName('update_agenda')
                .setDescription(
                    'Supprime les anciens messages, ping et poste dans le canal de weekly'
                )
        ),
    execute,
}
