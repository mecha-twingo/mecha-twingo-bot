import 'dotenv/config'
import { SlashCommandBuilder } from 'discord.js'
import { shutUp as execute } from '#actions'

export default {
    data: new SlashCommandBuilder()
        .setName('tg')
        .setDescription(
            'Invoque le pote MechaTwingo pour lui dire de la fermer de manière anonyme (ou pas)'
        )
        .addUserOption((option) =>
            option
                .setName('target')
                .setDescription('La target')
                .setRequired(true)
        ),
    execute,
    options: {
        specific: true,
    },
}
 