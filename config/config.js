import { createRequire } from 'node:module'
import 'dotenv/config'

const require = createRequire(import.meta.url)
const config = require('./config.json')

import CHARACTERS from './characters.js'
import MONTHS from './months.js'
import DAYS from './days.js'

const {
    DISCORD_TOKEN,
    NODE_ENV,
    BOT_CLIENT_ID,
    CAPTCHA_TOKEN,
    SCRIPT,
    STARTGG_TOKEN,
    MONGO_URL,
    USE_LOCAL_SCRAPPER
} = process.env

const mandatoryFields = {
    DISCORD_TOKEN,
    ...(SCRIPT === 'serve.js' && { STARTGG_TOKEN }),
    ...(SCRIPT === 'serve.js' && { CAPTCHA_TOKEN }),
}

Object.entries(mandatoryFields).map(([key, value]) => {
    if (!value) {
        throw new Error(`${key} is missing`)
    }
})

export default {
    ...config.default,
    ...(NODE_ENV === 'development' ? config.development : {}),
    ...(NODE_ENV === 'testing' ? config.testing : {}),
    ...(NODE_ENV === 'production' ? config.production : {}),
    ...(BOT_CLIENT_ID && { BOT_CLIENT_ID }),
    ...(MONGO_URL && { MONGO_URL }),
    ...(USE_LOCAL_SCRAPPER && { USE_LOCAL_SCRAPPER: USE_LOCAL_SCRAPPER === 'true' }), 
    MONTHS,
    DAYS,
    DISCORD_TOKEN,
    CAPTCHA_TOKEN,
    NODE_ENV,
    CHARACTERS,
    STARTGG_TOKEN,
}
