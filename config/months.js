export default {
    January: {
        label: 'Janvier',
        color: '#FFB3BA',
    },
    February: {
        label: 'Février',
        color: '#FFDFBA',
    },
    March: {
        label: 'Mars',
        color: '#FFFFBA',
    },
    April: {
        label: 'Avril',
        color: '#BAFFC9',
    },
    May: {
        label: 'Mai',
        color: '#BAE1FF',
    },
    June: {
        label: 'Juin',
        color: '#D5BAFF',
    },
    July: {
        label: 'Juillet',
        color: '#FFBAF3',
    },
    August: {
        label: 'Août',
        color: '#FFABAB',
    },
    September: {
        label: 'Septembre',
        color: '#FFDAAB',
    },
    October: {
        label: 'Octobre',
        color: '#FFFFAB',
    },
    November: {
        label: 'Novembre',
        color: '#B2E6FF',
    },
    December: {
        label: 'Décembre',
        color: '#C9D5FF',
    },
}
