import { REST, Routes } from 'discord.js'
import fs from 'node:fs'
import path from 'node:path'

import config from '#config'
const { DISCORD_TOKEN, BOT_CLIENT_ID } = config

const __dirname = path.resolve()

const commands = []

// Grab all the command files from the commands directory you created earlier
const foldersPath = path.join(__dirname, '/commands')
const commandsFolderContent = fs.readdirSync(foldersPath)

for (const commandFile of commandsFolderContent) {
    // Grab all the command files from the commands directory you created earlier
    const filePath = path.join(foldersPath, commandFile)
    const { default: command } = await import(filePath)

    if (
        'data' in command &&
        'execute' in command &&
        !command.options?.specific
    ) {
        commands.push(command.data.toJSON())
    }
}

// Construct and prepare an instance of the REST module
const rest = new REST().setToken(DISCORD_TOKEN);

// and deploy your commands!
(async () => {
    try {
        console.log(
            `Started refreshing ${commands.length} application (/) commands.`
        )

        // The put method is used to fully refresh all commands in the guild with the current set
        const data = await rest.put(Routes.applicationCommands(BOT_CLIENT_ID), {
            body: commands,
        })

        console.log(
            `Successfully reloaded ${data.length} application (/) commands.`
        )
    } catch (error) {
        // And of course, make sure you catch and log any errors!
        throw Error(error)
    }
})()
