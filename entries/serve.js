import { Client, Collection, GatewayIntentBits } from 'discord.js'
import fs from 'node:fs'
import path from 'node:path'
import mongoose from 'mongoose'
import config from '#config'

const { DISCORD_TOKEN, MONGO_URL, NODE_ENV } = config

const __dirname = path.resolve()
const client = new Client({
    intents: [
        GatewayIntentBits.Guilds,
        GatewayIntentBits.GuildMessages,
        GatewayIntentBits.MessageContent,
    ],
})


// Parsing Commands
client.commands = new Collection()
const foldersPath = path.join(__dirname, 'commands')
const commandsFolderContent = fs.readdirSync(foldersPath)

for (const commandFile of commandsFolderContent) {
    const filePath = path.join(foldersPath, commandFile)
    const { default: command } = await import(filePath)
    if ('data' in command && 'execute' in command) {
        client.commands.set(command.data.name, command)
    } else {
            console.log(
                `[WARNING] The command at ${filePath} is missing a required "data" or "execute" property.`
            )
    }
}

// Parsing Events
const eventsPath = path.join(__dirname, 'events')
const eventFiles = fs
    .readdirSync(eventsPath)
    .filter((file) => file.endsWith('.js'))

for (const file of eventFiles) {
    const filePath = path.join(eventsPath, file)
    const { default: event } = await import(filePath)
    if (event.once) {
        client.once(event.name, (...args) => event.execute(...args))
    } else {
        client.on(event.name, (...args) => event.execute(...args))
    }
}

async function main() {
    await mongoose.connect(`mongodb://${MONGO_URL}:27017/mecha-twingo${NODE_ENV === 'production' ? '' : '-testing'}`)
    await client.login(DISCORD_TOKEN)
}

process.env.TZ = 'Europe/Paris'
new Date().getTimezoneOffset()

main().catch((err) => console.log(err))
