#!/usr/bin/env bash

set -e

if ([ ! -e node_modules ] && [ ! "$NODE_ENV" != "development" ]) 
then
  npm i
fi

case $NODE_ENV in
  "development")
    ./node_modules/.bin/nodemon entries/$SCRIPT
    ;;

  "testing")
  node entries/$SCRIPT
    ;;

  "production")
  node entries/$SCRIPT
    ;;
esac
