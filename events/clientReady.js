import { Events } from 'discord.js'
import { commandsHandler, cronHandler } from '#helpers'
import { guildService } from '#services'; 

export default {
    name: Events.ClientReady,
    async execute(client) {
        const guilds = await guildService.getAll();

        await cronHandler.startGuildsCrons(client, guilds);
        await commandsHandler.updateCustomCommands(guilds);

        console.log('Bot successfully started !')
    },
}
