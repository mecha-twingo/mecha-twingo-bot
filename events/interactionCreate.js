import { Events } from 'discord.js'
import { guildService } from '#services'

import { goToThemesMenu } from '#pages'

export default {
    name: Events.InteractionCreate,
    async execute(interaction) {
        if (!interaction.isChatInputCommand()) {
            const event = interaction.customId
            if (event === 'configGuild') {
                await interaction.reply({
                    content: 'loading...',
                    ephemeral: true,
                })
                const guild = await guildService.getOne(interaction.guildId)

                if (!guild) {
                    await guildService.initialize(interaction.guildId)
                    await goToThemesMenu(interaction, {
                        previousMessage:
                            'Bienvenue sur le panneau de configuration de **Mecha Twingo** 😇',
                    })
                } else {
                    await goToThemesMenu(interaction)
                }
            }
            return
        }

        const command = interaction.client.commands.get(interaction.commandName)
        if (!command) {
            console.error(
                `No command matching ${interaction.commandName} was found.`
            )
            return
        }

        try {
            interaction.member.guild.config = await guildService.getOne(
                interaction.member.guild.id
            )
            await command.execute(interaction)
        } catch (error) {
            console.error(`Error executing ${interaction.commandName}`)
            console.error(error)
        }
    },
}
