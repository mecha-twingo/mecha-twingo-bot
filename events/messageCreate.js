import { Events } from 'discord.js'
import { guildService } from '#services'

import config from '#config'
const { TWINGO } = config

export default {
    name: Events.MessageCreate,
    async execute(message) {
        if (
            message.content.toLowerCase().search('quoi') >= 0 &&
            message.author.id !== TWINGO &&
            !message.author.bot
        ) {
            const guild = await guildService.getOne(message.guildId)
            if (guild.features.miscs.autoFeur.activated) await message.reply('FEUR !')
        }
        return
    },
}
