import emojiRegex from 'emoji-regex'
import axios from 'axios'

import { guildService } from '#services'

import config from '#config'
const { BOT_CLIENT_ID } = config

import {
    getResponseUrl,
    getTournamentInfos,
    dateBuilder,
    updateTournamentMessage,
    isActivated,
} from '#helpers'

const urlRegex =
    /^https:\/\/(?:www.)?start.gg\/(?:tournament\/)?((?:[a-z]|[0-9]|-)+)(?:\/details)?$/

export default async (interaction) => {
    await interaction.deferReply({ ephemeral: true })

    const { emoji, url, channel_id } =
        interaction.options._hoistedOptions.reduce(
            (res, { name, value }) => ({ ...res, [name]: value }),
            {}
        )

    if (!emojiRegex().test(emoji))
        return await interaction.editReply(`Emoji \`${emoji}\` non valide`)

    if (!urlRegex?.test(url))
        return await interaction.editReply(
            `Url de tournoi \`${url}\` non valide. L'Url doit être un url raccourcis ou doit pointer sur la page d'accueil d'un tournoi`
        )

    const tournamentLink = await getResponseUrl(url)
    await axios
        .get(await getResponseUrl(tournamentLink))
        .catch(
            async () =>
                await interaction.editReply(
                    `Aucun tournoi trouvé à l'url \`${url}\``
                )
        )

    const [, tournamentName] = urlRegex.exec(tournamentLink)
    const tournamentData = {
        ...(await getTournamentInfos(tournamentName)),
        emoji,
    }
    const name = `${emoji}  ${tournamentData.name} - ${dateBuilder(
        tournamentData.startAt,
        tournamentData.endAt
    )}`
    const content = `https://start.gg/${tournamentData.slug}`

    const guild = await guildService.getOne(interaction.guildId)

    const calendarChannelId =
        guild.features.monthliesMessages.calendar.channelId
    const tournamentsForumId =
        guild.features.monthliesMessages.tournaments.channelId

    if (
        !isActivated(guild, 'monthliesMessages') ||
        !calendarChannelId ||
        !tournamentsForumId
    ) {
        return await interaction.editReply(
            `Cette opération ne peut être réalisée`
        )
    }

    let channelId

    if (channel_id) {
        let thread;
        try {            
            thread = await interaction.client.channels.cache.get(tournamentsForumId).threads.fetch(channel_id)
        } catch {
            return await interaction.editReply(
                `L'identifiant ${channel_id} ne correspond à aucun fil dans le forum de tournoi`
            )
        }

        if (BOT_CLIENT_ID === thread.ownerId) {
            const firstMessage = await thread.fetchStarterMessage()
            firstMessage.edit(content)
            thread.setArchived(false)
            thread.setLocked(false)
            thread.setName(name)
        }

        channelId = thread.id
    } else {
        channelId = await interaction.client.channels.cache
            .get(tournamentsForumId)
            .threads.create({
                name,
                autoArchiveDuration: 60,
                message: {
                    content,
                },
                //appliedTags: ['offline']
            })
    }

    const updatedGuild = await guildService.insertTournament(
        interaction.guildId,
        { ...tournamentData, channelId }
    )

    await updateTournamentMessage(interaction, updatedGuild)

    return await interaction.editReply({
        content: ':white_check_mark:',
        ephemeral: true,
    })
}
