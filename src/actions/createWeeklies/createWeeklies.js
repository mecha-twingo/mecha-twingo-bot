import { guildService } from '#services'

import { isTwingo } from '#helpers'

import { handleCreation, validateWeekliesId } from './tools/index.js'

export default async (interaction) => {
    if (!isTwingo(interaction))
        return interaction.reply({
            content: `Désolé mais pour l'instant, seule la sainte twingo peut faire cette commande`,
            ephemeral: true,
        })

    try {
        const weekliesIds = validateWeekliesId(interaction)

        await interaction.deferReply({ ephemeral: true })

        const guild = await guildService.getOne(interaction.guildId)

        const { type, message } = await handleCreation({ guild, weekliesIds })

        await interaction.editReply({
            content:
                type === 'success'
                    ? 'Terminé ! :partying_face:'
                    : message || 'Erreur ! :cry:',
        })

        if (type === 'success') {
            await interaction.followUp(message)
        }
    } catch ({ message }) {
        await interaction.reply({ content: message, ephemeral: true })
    }
}
