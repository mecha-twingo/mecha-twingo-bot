import axios from 'axios'

import { dockerCommands, getResponseUrl } from '#helpers'

import config from '#config'
const { GSHEET_URL } = config

const regex = /Creation of tournament ((?:[0-9])+): (Success|Failed){1}/

const getFinalMessage = (tournaments, statusMessage, createdTournaments) =>
    `Demande de création de **${tournaments}** tournoi${tournaments > 1 ? 's' : ''} !

    ${statusMessage}

    ${(() => {
        if (createdTournaments === 0)
            return `${tournaments === 1 ? "Le tournoi n'a pas pu" : "Aucun tournoi n'a pu"} être créé :neutral_face:`
        else if (createdTournaments === 1 && tournaments > 1)
            return "La création d'un tournoi s'est déroulée avec succès"
        else if (createdTournaments === 1 && tournaments === 1)
            return "La création du tournoi s'est déroulée avec succès :grin:"
        else if (createdTournaments != tournaments)
            return `La création de ${createdTournaments} tournois sur ${tournaments} s'est déroulée avec succès`
        else if (createdTournaments > 1 && createdTournaments === tournaments)
            return `La création des tournois s'est déroulée avec succès :grin:`
    })()}`

export default async ({ guild, weekliesIds }) => {
    try {
        const { data: events } = await axios
            .get(`${GSHEET_URL}${guild.googleSheetId}`)
            .catch(() => {
                throw Error('GoogleSheet inaccessible ou introuvable')
            })

        if (!events) throw Error('Aucun évenement trouvé')

        if (weekliesIds === '*')
            weekliesIds = events.reduce(
                (res, cur) =>
                    cur.isActive && cur.creationAuto
                        ? `${res}${res === '' ? '' : ','}${cur.id}`
                        : res,
                ''
            )

        if (!weekliesIds?.length)
            throw Error(
                "Aucun tournoi avec la génération automatique n'est activé"
            )

        let createdTournaments = 0
        await dockerCommands('pull')

        if ((await dockerCommands('ps', guild))?.raw !== '') {
            throw Error(
                "Le scrapper de StartGG est déjà en cours d'utilisation :zap: "
            )
        }

        const statusMessage = (
            await dockerCommands('run', guild, 'creation', weekliesIds)
        ).containerId
            .split('\n')
            .reduce(async (res, cur) => {
                if (!regex.exec(cur)) return await res

                const [, id, status] = regex.exec(cur)

                const event = events.find((event) => event.id === id)

                return `${(await res) === '' ? '' : (await res) + '\n'}- Création du tournoi **${id}${
                    status.toLowerCase() === 'success'
                        ? await (async () => {
                              createdTournaments++
                              return `/ ${event.title}**  :white_check_mark:\t:point_right: <${await getResponseUrl(event.shortLink)}>`
                          })()
                        : `/ ${event?.title ? event.title : '__Tournoi introuvable__'}**  :no_entry_sign:`
                }`
            }, Promise.resolve(''))

        return {
            type: 'success',
            message: getFinalMessage(
                weekliesIds.split(',').length,
                await statusMessage,
                createdTournaments
            ),
        }
    } catch (e) {
        return { type: 'error', message: e.message }
    }
}
