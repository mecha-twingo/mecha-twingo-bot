  export default (interaction) => {
    const wrondDataErrorMessage =
        "Seul le caractère `*` ou une liste d'`id` peuvent être prit en paramètre"

    const options = interaction.options._hoistedOptions.reduce(
        (res, { name, value }) => ({ ...res, [name]: value }),
        {}
    )

    const weeklies_id = options.weeklies_id
        .replaceAll(' ', '')
        .split(',')
        .reduce((res, id) => {
            if (id && !/^(([1-9]*[0-9]{1})|\*)$/.test(id))
                throw Error(wrondDataErrorMessage)
            return id ? `${res}${res === '' ? '' : ','}${id}` : res
        }, '')

    if (weeklies_id.length > 1 && weeklies_id.includes('*'))
        throw Error(wrondDataErrorMessage)

    return weeklies_id
}
