import { guildService } from '#services'

import { isTwingo } from '#helpers'

import { handleDeletion, validateTournamentDeletion } from './tools/index.js'

export default async (interaction) => {
    if (!isTwingo(interaction))
        return interaction.reply({
            content: `Désolé mais pour l'instant, seule la sainte twingo peut faire cette commande`,
            ephemeral: true,
        })

    try {
        const tournamentsUrls = validateTournamentDeletion(interaction)

        await interaction.deferReply({ ephemeral: true })

        const guild = await guildService.getOne(interaction.guildId)

        const { type, message } = await handleDeletion({
            guild,
            tournamentsUrls,
        })
        await interaction.editReply({
            content:
                (await type) === 'success'
                    ? 'Terminé ! :partying_face:'
                    : 'Erreur ! :cry:',
        })

        await interaction.followUp(await message)
    } catch ({ message }) {
        await interaction.reply({ content: message, ephemeral: true })
    }
}
