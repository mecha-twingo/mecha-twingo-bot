import { dockerCommands } from '#helpers'

const regex =
    /Deletion of tournament (?:https:\/\/(?:www\.)?start\.gg\/(?:(?:admin\/)?tournament\/)?)?((?:[a-z0-9]|-)+)(?:[a-z0-9]|-|\/)*: (Success|Failed){1}(\(not found\))?/

const getFinalMessage = (tournaments, statusMessage, deletedTournaments) =>
    `Demande de suppression de **${tournaments}** tournoi${tournaments > 1 ? 's' : ''} !

${statusMessage}

${(() => {
    if (deletedTournaments === 0)
        return `${tournaments === 1 ? "Le tournoi n'a pas pu" : "Aucun tournoi n'a pu"} être supprimé :neutral_face:`
    else if (deletedTournaments === 1 && tournaments > 1)
        return "La suppression d'un tournoi s'est déroulée avec succès"
    else if (deletedTournaments === 1 && tournaments === 1)
        return "La suppression du tournoi s'est déroulée avec succès :grin:"
    else if (deletedTournaments != tournaments)
        return `La suppression de ${deletedTournaments} tournois sur ${tournaments} s'est déroulée avec succès`
    else if (deletedTournaments > 1 && deletedTournaments === tournaments)
        return `La suppression des tournois s'est déroulée avec succès :grin:`
})()}`

export default async ({ guild, tournamentsUrls }) => {
    try {
        let deletedTournaments = 0

        await dockerCommands('pull')

        if ((await dockerCommands('ps', guild))?.raw)
            return {
                type: 'error',
                message:
                    "Le scrapper de StartGG est déjà en cours d'utilisation :zap: ",
            }

        const statusMessage = ( 
            await dockerCommands('run', guild, 'deletion', tournamentsUrls)
        ).containerId
            .split('\n')
            .reduce(
                (res, cur) =>
                    regex.exec(cur)
                        ? `${res === '' ? '' : res + '\n'}- Tournoi : **${regex.exec(cur)[1]}** ${
                              regex.exec(cur)[2].toLowerCase() === 'success'
                                  ? (() => {
                                        deletedTournaments++
                                        return ' :white_check_mark:'
                                    })()
                                  : ' :no_entry_sign:'
                          } `
                        : res,
                ''
            )

        return {
            type: 'success',
            message: getFinalMessage(
                tournamentsUrls.split(',').length,
                statusMessage,
                deletedTournaments
            ),
        }
    } catch (e) {
        return { type: 'error', message: e.message }
    }
}
