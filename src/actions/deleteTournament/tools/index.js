import handleDeletion from './handleDeletion.js'
import validateTournamentDeletion from './validateTournamentDeletion.js'

export { handleDeletion, validateTournamentDeletion }
