import config from '#config'
const { TWINGO } = config

const regex =
    /^https:\/\/(?:www.)?start.gg\/(?:(?:admin\/)?tournament\/)?((?:[a-z]|[0-9]|-)+)(?:\/.*)?$/

export default (interaction) => {
    const options = interaction.options._hoistedOptions.reduce(
        (res, { name, value }) => ({ ...res, [name]: value }),
        {}
    )

    const tournamentsUrls = options.tournaments_url
        .replaceAll(' ', '')
        .split(',')
        .reduce((res, url) => {
            if (url && !regex.test(url))
                throw Error(`Url de tournoi ${url} non valide`)
            return url ? `${res}${res === '' ? '' : ','}${url}` : res
        }, '')

    if (interaction.member.id !== TWINGO)
        throw Error(
            "Désolé mais pour l'instant, seule la sainte twingo peut faire cette commande"
        )

    return tournamentsUrls
}
