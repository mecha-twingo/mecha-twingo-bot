import axios from 'axios'

import { guildService } from '#services'

import { getResponseUrl, updateTournamentMessage, isActivated } from '#helpers'

const urlRegex =
    /^https:\/\/(?:www.)?start.gg\/(?:tournament\/)?((?:[a-z]|[0-9]|-)+)(?:\/details)?$/

export default async (interaction) => {
    await interaction.deferReply({ ephemeral: true })

    const { url } = interaction.options._hoistedOptions.reduce(
        (res, { name, value }) => ({ ...res, [name]: value }),
        {}
    )

    if (!urlRegex.test(url))
        return await interaction.editReply(
            `Url de tournoi \`${url}\` non valide. L'Url doit être un url raccourcis ou doit pointer sur la page d'accueil d'un tournoi`
        )

    const tournamentLink = await getResponseUrl(url)
    await axios
        .get(await getResponseUrl(tournamentLink))
        .catch(
            async () =>
                await interaction.editReply(
                    `Aucun tournoi trouvé à l'url \`${url}\``
                )
        )

    const [, tournamentName] = urlRegex.exec(tournamentLink)

    const tournament = await guildService.getTournamentsBySlug(
        interaction.guildId,
        tournamentName
    )
    const guild = await guildService.getOne(interaction.guildId)
    const tournamentChannelId =
        guild.features.monthliesMessages.tournaments.channelId

    if (
        !isActivated(guild, 'monthliesMessages') ||
        !tournament ||
        !tournamentChannelId
    ) {
        return await interaction.editReply(
            `Cette opération ne peut être réalisée`
        )
    }

    const channel = interaction.client.channels.cache.get(tournamentChannelId)

    const res = [...channel.threads.cache].find(
        ([id]) => id === tournament.channelId
    )
    if (
        res &&
        (await guildService.isChannelIdUsedOnce(
            interaction.guildId,
            tournament.channelId
        ))
    ) {
        const [, thread] = res
        thread.setLocked(true)
        thread.setArchived(true)
    }

    const updatedGuild = await guildService.deleteTournament(
        interaction.guildId,
        tournamentName
    )

    await updateTournamentMessage(interaction, updatedGuild)

    return await interaction.editReply({
        content: ':white_check_mark:',
        ephemeral: true,
    })
}
