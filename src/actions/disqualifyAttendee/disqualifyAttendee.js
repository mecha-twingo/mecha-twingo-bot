import axios from 'axios'

import { getResponseUrl, getTournamentInfos } from '#helpers'
import { handleDisqualification } from './tools/index.js'

import { guildService } from '#services'

export default async (interaction) => {
    try {
        const participantIds = interaction.options._hoistedOptions.reduce(
            (acc, { value }) => ([ ...acc, value ]),
            []
        )
        await interaction.deferReply({ ephemeral: true })

        const guild = await guildService.getOne(interaction.guildId)
        
        if(!guild || !guild.googleSheetId) {
            throw 'Ce serveur n\'a pas configurer mecha twingo'

        }

        const { data: events } = await axios.get(
            `https://strelitzia.eltwingo.xyz/gsheet-reader/${guild.googleSheetId}`
        )
        const weekly = events.find(e => e.channelId === interaction.channelId)
        
        if(!weekly) {
            throw 'Aucune weekly n\'est liée à ce canal'
        }

        const longSlug = await getResponseUrl(weekly.shortLink)
        const tournamentData = await getTournamentInfos(longSlug.split('/')[longSlug.split('/').length-2])

        const attendeeIdList = participantIds.reduce((acc, cur) => {
            const participant = tournamentData.participants.nodes.find(({ requiredConnections }) =>
                requiredConnections.find(({ type, externalId }) => type === 'DISCORD' && externalId === cur))
            return  { ...acc, [participant ? participant.requiredConnections.find(({ type, externalId }) => type === 'DISCORD' && externalId === cur).externalId : cur]: participant?.id.toString() }
        }, {})

        if(!Object.values(attendeeIdList).filter(a => a).length) {
            throw 'Aucun participant sur le start.gg n\'a été trouvé à partir des utilisateurs discord de la commande'
        }
        
        await interaction.editReply(
            await handleDisqualification({
                guild,
                tournamentUrl: longSlug,
                attendeeIdList,
                total: participantIds.length
            })
        )
        
    } catch (message) {
        console.log(message)
        await interaction.editReply(`${message} 😔`)
    }

}
