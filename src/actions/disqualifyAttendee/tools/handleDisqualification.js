import { dockerCommands } from '#helpers'

export default async ({ guild, tournamentUrl, attendeeIdList, total }) => {
    
    await dockerCommands('pull')
    if ((await dockerCommands('ps', guild))?.raw) {
        throw Error("Le scrapper de StartGG est déjà en cours d'utilisation :zap: ")
    }
        
    const commandLogs = (
        await dockerCommands('run', guild, 'disqualification', `${tournamentUrl},${Object.values(attendeeIdList).map(a => a)}`)
    ).containerId.split('\n')

    const disqualifiedParticipantIds = commandLogs.reduce((acc, cur) => (
        cur.match(/[^[0-9]+([0-9]+): Success/) ? [ ...acc, cur.match(/[^[0-9]+([0-9]+): Success/)[1].toString()] : acc
    ), [])

    const contentMessage = `${Object.entries(attendeeIdList).reduce(
        (acc, [discordId, participantId]) => 
            `${acc}- <@${discordId}> ${participantId && disqualifiedParticipantIds.includes(participantId) ? '✅' : '🚫'}\n`, '\n'
    )}`;

    return `
Demande de disqualification de **${total}** participant${total > 1 ? 's' : ''} !
${contentMessage}
${(() => {
    if (disqualifiedParticipantIds.length === 0)
        return `${total === 1 ? "Le participant n'a pas pu" : "Aucun participant n'a pu"} être disqualifié :neutral_face:`
    else if (disqualifiedParticipantIds.length === 1 && total > 1)
        return "La disqualification d'un participant s'est déroulée avec succès"
    else if (disqualifiedParticipantIds.length === 1 && total === 1)
        return "La disqualification du participant s'est déroulée avec succès :grin:"
    else if (disqualifiedParticipantIds.length > 0 && disqualifiedParticipantIds.length < total)
        return `La disqualification de ${disqualifiedParticipantIds.length} participants sur ${total} s'est déroulée avec succès`
    else if (disqualifiedParticipantIds.length > 0 && disqualifiedParticipantIds.length === total)
        return `La disqualification des participants s'est déroulée avec succès :grin:`
})()}`

}
