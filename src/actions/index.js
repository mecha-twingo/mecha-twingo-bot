import createWeeklies from './createWeeklies/createWeeklies.js'
import deleteTournaments from './deleteTournament/deleteTournaments.js'
import postMsgWeeklies from './postWeekliesMessages/postWeekliesMessages.js'
import settings from './settings/settings.js'
import shutUp from './shutUp/shutUp.js'
import addTournamentMessage from './addTournamentMessage/addTournamentMessage.js'
import refreshTournamentMessage from './refreshTournamentMessage/refreshTournamentMessage.js'
import deleteTournamentMessage from './deleteTournamentMessage/deleteTournamentMessage.js'
import disqualifyAttendee from './disqualifyAttendee/disqualifyAttendee.js';

export {
    createWeeklies,
    deleteTournaments,
    postMsgWeeklies,
    settings,
    shutUp,
    addTournamentMessage,
    deleteTournamentMessage,
    refreshTournamentMessage,
    disqualifyAttendee
}
