import { handlePostAgenda } from './tools/index.js'

import { guildService } from '#services'

import config from '#config'
const { TWINGO } = config

export default async (interaction) => {
    const guild = await guildService.getOne(interaction.guildId)
    
    const updateAgenda = !!interaction?.options._hoistedOptions[0]?.value

    if (updateAgenda && interaction.member.id !== TWINGO)
        return await interaction.reply({
            content: `Désolé mais pour l'instant, seule la sainte twingo peut faire cette commande`,
            ephemeral: true,
        })

    await interaction.deferReply()
    
    await handlePostAgenda({
        client: interaction.client,
        guild,
        interaction,
    })
        
    await interaction.editReply({
        content: `${updateAgenda ? `Le channel <#${guild.features.weekliesMessages.agenda?.channelId}> à été vidé et les` : '🔽 Les'} nouveaux messages ont été postés ${updateAgenda ? 'dedans' : 'en dessous 🔽'}`,
    })
}
