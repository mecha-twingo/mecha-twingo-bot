import axios from 'axios'

import { getResponseUrl } from '#helpers'
import { ping, intro, postEventMsg, outro } from './messages.js'

import config from '#config'
const { GSHEET_ID } = config

export default async ({ client, guild, interaction, updateAgenda }) => {
    const shouldUpdateAgenda = !!interaction?.options._hoistedOptions[0]?.value || updateAgenda;

    const channelId = shouldUpdateAgenda
    ? guild.features.weekliesMessages.agenda?.channelId
    : interaction?.channel.id

    const chan = client.channels.cache.get(channelId)

    if (!chan) {
        return;
    }

    if (shouldUpdateAgenda) {
        const msgs = await chan.messages.fetch({ count: 20 })
        chan.bulkDelete(msgs, true)
        chan.send(ping(guild.features.weekliesMessages.agenda?.roleId))
    }
    chan.send(intro)

    const { data: events } = await axios.get(
        `https://strelitzia.eltwingo.xyz/gsheet-reader/${GSHEET_ID}`
    )
    const updatedEvents = Promise.all(
        events.map(async (event) => ({
            ...event,
            link: event.shortLink
                ? await getResponseUrl(event.shortLink)
                : event.customLink,
        }))
    )
    for (const event of await updatedEvents) {
        if (
            event.isActive &&
            (event.customLink || (event.creationAuto && event.shortLink))
        )
            await chan.send(await postEventMsg(event))
    }

    chan.send(outro)
}
