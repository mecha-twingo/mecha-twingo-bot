export default async ({ client, guild }) => {
    const channelId = guild.features.weekliesMessages.staff?.channelId;

    const chan = client.channels.cache.get(channelId)

    if (!chan) {
        return;
    }

    chan.send(`<@&${guild.features.weekliesMessages.staff?.roleId}> salut les gitans, je créer les liens dans **2h** et je poste le message dans **4h**. Assurez vous que le gSheet est à jour ! Je vous remets le lien ici: https://docs.google.com/spreadsheets/d/${guild.googleSheetId}/edit?usp=drivesdk`);
}
