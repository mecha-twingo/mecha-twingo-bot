import handlePostAgenda from './handlePostAgenda.js';
import handlePostStaff from './handlePostStaff.js';

export { handlePostAgenda, handlePostStaff }
