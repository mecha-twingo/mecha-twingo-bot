const ping = (roleId) => `
Hey <@&${roleId}> !
`

const intro = `
🔽 Les liens d'inscriptions des weeklies de la semaine prochaine sont dispos ! 🔽

** **
`

const outro = `
Pensez à prendre vos consoles pour un maximum de freeplay et votre casque & splitter audio pour plus de confort pour vos games !
Les joueurs GCN, prenez votre propre adaptateur !

En cas de retard ou d'absence, prévenez les organisateurs au plus vite ! :pray:

Excellent week-end à vous !
`

const onlineEvent = async (event) => `
    > ** **
    > ${event.icon} __**${event.title} - Online 📡**__
    > 
    > __${event.day} soir__: ${event.type} !\
    ${
        event.info
            ? `
    > 
    > 👉  **${event.info}**
    > 
    `
            : ``
    }\
    > 🕔  Début du tournoi à __${event.startAt}__
    > 🚹  **${event.maxPlayers} joueurs max**
    ${
        event.stream
            ? `
    > 📺  Stream sur <${event.stream}>`
            : ``
    }\
    > 
    > Inscription: <${event.creationAuto ? event.link : event.customLink}}>
    > Discussion: <#${event.channelId}>
    > 
    ** **
`

const offlineEvent = async (event) => `
    > ** **
    > ${event.icon} __**${event.title} - ${event.city}**__
    > 
    > __${event.day} soir__: ${event.type} !
    > \
    ${
        event.info
            ? `
    > 👉  **${event.info}**
    > `
            : ``
    }
    > 🏛️  Ouverture des lieux à **${event.openAt}** et fermeture à **${event.closeAt}** 
    > 📍  ${event.address}
    > 🎮  __Freeplay:__ ${event.freeplay}\
    ${
        !(event.startAt === '' || event.type.toLowerCase() === 'freeplay')
            ? `
    > 🕔  Début du tournoi à __${event.startAt}__`
            : ``
    }
    > 💻  **${event.setups}** setups disponibles
    > 🚹  **${event.maxPlayers} joueurs max**
    > 📢  ${event.price}\
    ${
        event.hasFood
            ? `
    > 🥪  Restauration sur place possible`
            : ``
    }\
    ${
        event.stream
            ? `
    > 📺  Stream sur <${event.stream}>`
            : ``
    }
    > 
    > Inscription: <${event.creationAuto ? event.link : event.customLink}>
    > Discussion: <#${event.channelId}>
    > 
    ** **
    `

const postEventMsg = async (event) =>
    event.isOnline ? await onlineEvent(event) : await offlineEvent(event)

export { ping, intro, postEventMsg, outro }
