import { updateTournamentMessage } from '#helpers'

import { guildService } from '#services'

export default async (interaction) => {
    await interaction.deferReply({ ephemeral: true })

    const updatedGuild = await guildService.deletePreviousTournaments(interaction.guildId)

    await updateTournamentMessage(interaction, updatedGuild)

    return await interaction.editReply({
        content: ':white_check_mark:',
        ephemeral: true,
    })
}
