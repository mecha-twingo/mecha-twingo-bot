import { ButtonBuilder, ButtonStyle, ActionRowBuilder } from 'discord.js'

import { isTwingo } from '#helpers';

export const parseMessage = () => ({
    content: '### __Panneau de configuration__  :arrow_lower_left:\n' + '‎',
    components: [
        new ActionRowBuilder().addComponents(
            new ButtonBuilder()
                .setCustomId('configGuild')
                .setLabel('Commencer 🛠️')
                .setStyle(ButtonStyle.Primary)
                .setDisabled(false)
        ),
    ],
})

export default async (interaction) => {
    if (!isTwingo(interaction))
        return interaction.reply({ content: `Désolé mais pour l'instant, seule la sainte twingo peut faire cette commande`, ephemeral: true });

    const currentChan = interaction.client.channels.cache.get(
        interaction.channelId
    )
    await currentChan.send({
        files: ['./banner.png'],
    })
    await currentChan.send(parseMessage())

    await interaction.reply({ content: '✅ ‎', ephemeral: true })
}
