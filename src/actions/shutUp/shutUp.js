import config from '#config'
const { TWINGO } = config

import { guildService } from '#services'

import { getMessages } from './tools/index.js'

export default async (interaction) => {
    const { id: userId, username: userName } = interaction.user
    const { id: targetId, username: targetName } =
        interaction.options._hoistedOptions[0].user

    const guild = await guildService.getOne(interaction.guildId)
    if (!guild.features.shutUp)
        return await interaction.reply({
            content: "Cette commande n'est pas activé sur ce serveur",
            ephemeral: true,
        })

    if (guild.features.shutUp.cooldownedUsers.includes(userId))
        return await interaction.reply({
            content: "T'as déjà utilisé ton `/tg` journalier",
            ephemeral: true,
        })
    else if (userId !== TWINGO)
        await guildService.addUserInCooldown(interaction.guildId, userId)

    await interaction.reply({ content: '📨 ‎', ephemeral: true })

    if (targetId === TWINGO) {
        return (
            userId !== TWINGO &&
            interaction.client.channels.cache
                .get(interaction.channelId)
                .send(
                    `Ce gros gitan essaye de salir notre maître, honte à toi <@${userId}> !`
                )
        )
    }

    interaction.client.channels.cache
        .get(interaction.channelId)
        .send(await getMessages(targetId, userId))
    console.log(`/tg from ${userId}(${userName}) to ${targetId}(${targetName})`)

    return
}
