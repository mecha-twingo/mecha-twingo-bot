import axios from 'axios'

import config from '#config'
const { TWINGO } = config

export default async (to, from) => {
    const { data } = await axios.get(
        'https://opensheet.elk.sh/1v_vlbsYi89sGUdezQMyu3JoVXY--May72yrGnSoDx78/1'
    )
    const formattedData = data.reduce(
        (acc, cur) =>
            from === TWINGO && cur['Phrase de trash'].includes('{}')
                ? acc
                : [
                      ...acc,
                      cur['Phrase de trash']
                          .replaceAll('[]', `<@${to}>`)
                          .replaceAll('{}', `<@${from}>`),
                  ],
        []
    )
    return formattedData[Math.floor(Math.random() * formattedData.length)]
}
