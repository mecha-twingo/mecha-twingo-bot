import config from '#config'
const { BOT_CLIENT_ID, DISCORD_TOKEN } = config

import { Routes, REST } from 'discord.js'
import { isActivated } from './index.js'

const rest = new REST().setToken(DISCORD_TOKEN)

export const enableCommands = async (commandNameList, guildId) => {

    const body = await Promise.all(
        commandNameList.map(async (commandName) =>
                (await import(`../../commands/${commandName}.js`)).default.data.toJSON()
        )
    )
    return await rest.put(Routes.applicationGuildCommands(BOT_CLIENT_ID, guildId), { body })
}

export const disableCommands = async (commandNameList, guildId) => {
    const commands = await rest.get(
        Routes.applicationGuildCommands(BOT_CLIENT_ID, guildId)
    )

    return await Promise.all(commandNameList.map(async (commandName) => {
        commands.find(({ name }) => name === commandName) 
            && await rest.delete(
                    Routes.applicationGuildCommand(
                        BOT_CLIENT_ID,
                        guildId,
                        commands.find(({ name }) => name === commandName).id
                    )
                )
            }
    ))
}

export const updateCustomCommands = async (guilds) => {
    guilds.map(guild => {
        const commands = [];

        if(isActivated(guild, 'monthliesMessages')) {
            commands.push('add_tournament_message','delete_tournament_message','refresh_tournament_messages')
        }

        if(isActivated(guild, 'shutUp')) {
            commands.push('tg')
        }

        enableCommands(commands, guild.id)
    })
}