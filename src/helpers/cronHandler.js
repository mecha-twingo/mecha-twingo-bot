import { guildService } from '#services'

import { updateTournamentMessage, isActivated } from '#helpers';

import CronJobManager from 'cron-job-manager'

import { handlePostAgenda, handlePostStaff } from '../actions/postWeekliesMessages/tools/index.js'
import handleCreation from '../actions/createWeeklies/tools/handleCreation.js'

const manager = new CronJobManager()

export const updateGuildCrons = async (client, guild) => {
    const [weeklyKey, staffKey, generationKey] = [
        `${guild.id}-agenda`,
        `${guild.id}-staff`,
        `${guild.id}-generate`,
    ]

    if (manager.exists(weeklyKey)) {
        manager.deleteJob(weeklyKey)
    }

    if (manager.exists(staffKey)) {
        manager.deleteJob(staffKey)
    }

    if (manager.exists(generationKey)) {
        manager.deleteJob(generationKey)
    }

    if (isActivated(guild, 'weekliesMessages')) {
        manager.add(
            weeklyKey,
            `0 ${guild.features.weekliesMessages.agenda?.hour} * * ${guild.features.weekliesMessages.agenda?.day}`,
            async () =>
                await handlePostAgenda({
                    client,
                    guild,
                    updateAgenda: true
                }),
            { start: true, timeZone: 'Europe/Paris' }
        )
        manager.add(
            staffKey,
            `0 ${guild.features.weekliesMessages.staff?.hour} * * ${guild.features.weekliesMessages.staff?.day}`,
            async () =>
                await handlePostStaff({
                    client,
                    guild
                }),
            { start: true, timeZone: 'Europe/Paris' }
        )
    }

    if (isActivated(guild, 'weekliesGeneration')) {
        manager.add(
            generationKey,
            `0 ${guild.features.weekliesGeneration.generation?.hour} * * ${guild.features.weekliesGeneration.generation?.day}`,
            async () => {
                const chan = client.channels.cache.get(
                    guild.features.weekliesMessages.staff.channelId
                )
                chan.send('Je lance la création des liens ! :blue_car:')
                chan.send(
                    (await handleCreation({ guild, weekliesIds: '*' })).message
                )
            },
            { start: true, timeZone: 'Europe/Paris' }
        )
    }
}

export const startGuildsCrons = async (client, guilds) => {
    manager.add(
        'cooldownedUsers',
        `0 0 0 * * *`,
        async () => (await guildService.resetAllCooldowns()),
        { start: true, timeZone: 'Europe/Paris' }
    )

    manager.add(
        'tournamentsMeessagesCleaner',
        `0 0 0 15 * *`,
        async () => {
            await Promise.all(guilds.map(async (guild) => {
                await guildService.deletePreviousTournaments(guild.id)

                await updateTournamentMessage(
                    null,
                    guild,
                    client
                )
            }))
        },
        { start: true, timeZone: 'Europe/Paris' }
    )

    return await Promise.all(guilds.map(async (guild) => await updateGuildCrons(client, guild)))
}
