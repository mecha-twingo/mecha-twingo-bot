import moment from 'moment'

export default (startAt, endAt) => {
    const [startDay, startMonth, startYear] = moment(startAt)
        .locale('fr')
        .format('Do MMMM YYYY')
        .split(' ')
    const [endDay, endMonth, endYear] = moment(endAt)
        .locale('fr')
        .format('Do MMMM YYYY')
        .split(' ')
    const timeGap = moment(endAt).diff(moment(startAt), 'days')

    if (startMonth === endMonth) {
        if (timeGap === 0) return `le ${startDay} ${startMonth} ${endYear}`
        else if (timeGap === 1)
            return `les ${startDay} & ${endDay} ${startMonth} `
        else return `du ${startDay} au ${endDay} ${startMonth} ${endYear}`
    } else {
        if (timeGap === 1)
            return `les ${startDay} ${startMonth} & ${endDay} ${endMonth} ${endYear}`
        else
            return `du ${startDay} ${startMonth} au ${endDay} ${endMonth} ${startYear !== endYear ? endYear : ''}`
    }
}
