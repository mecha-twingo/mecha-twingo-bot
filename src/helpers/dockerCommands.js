import { dockerCommand } from 'docker-cli-js'

import config from '#config'
const { CAPTCHA_TOKEN, NODE_ENV, CONTAINER_REGISTRY, USE_LOCAL_SCRAPPER } = config

const startGGImage = !USE_LOCAL_SCRAPPER ? `${CONTAINER_REGISTRY}:${NODE_ENV === 'production' ? '' : 'testing-'}latest` : 'startgg-scrapper-local'

const commandsList = {
    run: (guild, mod, params) => `run \
    --name "${guild.id}" \
    --rm \
    -e STARTGG_EMAIL="${guild.features.weekliesGeneration.startgg.login}" \
    -e STARTGG_PASSWORD="${guild.features.weekliesGeneration.startgg.password}" \
    -e TOURNAMENT_PARAMS="${params}" \
    -e MOD="${mod}" \
    -e CAPTCHA_TOKEN="${CAPTCHA_TOKEN}" \
    ${NODE_ENV === 'development' ? '--add-host=host.docker.internal:host-gateway' : ''} \
    ${startGGImage}`,
    ps: (guild) => `ps -q --filter name=${guild.id}`,
    pull: () => `pull ${startGGImage}`,
}

export default async (cmd, guild, mod = null, params = null) => {
    try {
        if (USE_LOCAL_SCRAPPER && cmd === 'pull') return;
        if (cmd === 'pull') console.log('Pulling image ...')
        return await dockerCommand(commandsList[cmd](guild, mod, params), {
            echo: cmd !== 'ps',
        })
    } catch {
        switch (mod) {  
            case 'creation':
                throw Error(`La **création** de la référence de tournois a échoué`)
            case 'deletion':
                throw Error(`La **suppression** de tournois a échoué`)
            case 'disqualification':
                throw Error(`La **disqualification** du joueur a échoué`)
            default:
                if(cmd === 'ps') {
                    throw Error('La commande **ps** a échouée')
                }
                throw Error('La commande a échouée')

        }
    }
}
