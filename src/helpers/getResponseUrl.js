import axios from 'axios'

export default async (tournamentShortSlug) => {
    try {
        const tournament = await axios.get(tournamentShortSlug)
        return tournament.request.res.responseUrl
    } catch {
        return tournamentShortSlug
    }
}
