import axios from 'axios'
import moment from 'moment'

import config from '#config'
const { STARTGG_TOKEN } = config

const formatData = async (tournamnent) =>
    Object.entries(tournamnent).reduce(
        (acc, [key, value]) => ({
            ...acc,
            [key]: value,
            ...(key === 'startAt' && { [key]: moment.unix(value) }),
            ...(key === 'endAt' && { [key]: moment.unix(value) }),
        }),
        {}
    )

export default async (tournamentName) => {
    const { data } = await axios({
        url: 'https://api.start.gg/gql/alpha',
        method: 'post',
        headers: {
            'content-type': 'application/json',
            Authorization: `Bearer ${STARTGG_TOKEN}`,
        },
        data: {
            operationName: 'TournamentQuery',
            variables: {
                slug: tournamentName,
            },
            query: `query TournamentQuery($slug: String) {
                        tournament(slug: $slug){
                            id
                            name
                            startAt
                            endAt
                            city
                            slug
                            countryCode
                            participants(query: { page: 1, perPage: 500 }) {
                                nodes {
                                    id
                                    requiredConnections {
                                        externalId
                                        externalUsername
                                        type
                                    }
                                    player {
                                        gamerTag
                                    }
                                }
                            }
                        }
                    }`,
        },
    })
    return await formatData(data.data.tournament)
}
