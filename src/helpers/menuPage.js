export default async ({
    interaction,
    description,
    details = '',
    components,
    handle,
    props = { previousMessage: '' },
    customDeferUpdate = false,
}) => {
    try {
        const splittedDetails = details
            ?.split('\n')
            .reduce((acc, cur) => `${acc ? `${acc}\n` : acc}> ${cur}`, '')
        const { previousMessage } = props

        const response = await interaction.editReply({
            content:
                `${previousMessage ? `*${previousMessage}*\n` : ``}` +
                `# Tableau de bord 🚗\n` +
                `${details ? `${splittedDetails}\n` : ``}` +
                `> ${description}\n` +
                `‎`,
            components,
        })

        const confirmation = await response.awaitMessageComponent({
            time: 30000,
        })
        if (!customDeferUpdate) {
            confirmation.deferUpdate()
        }

        await handle(confirmation)
    } catch (e) {
        if (e.message.includes('reason: time')) {
            setTimeout(async () => await interaction.deleteReply(), 1500)
            await interaction.editReply({
                content:
                    `# Tableau de bord  🚗\n` +
                    `> Déconnexion pour inactivité 🌳\n` +
                    `‎`,
                ephemeral: true,
                components: [],
            })
        } else {
            console.log(e)
            await interaction.editReply({
                content: `# Tableau de bord  🚗\n` + `> ⚠️ ERREUR ⚠️\n` + `‎`,
                ephemeral: true,
                components: [],
            })
        }
    }
}
