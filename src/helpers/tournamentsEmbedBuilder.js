import { EmbedBuilder } from 'discord.js'

import config from '#config'
const { MONTHS } = config

import { dateBuilder } from '#helpers'

export default async (monthSortedTournaments) => {
    return Object.entries(monthSortedTournaments).reduce(
        (acc, [month, tournaments]) => {
            const startMonth = MONTHS[month]

            const embed = new EmbedBuilder()
                .setColor(startMonth.color)
                .setTitle(startMonth.label)
                .setDescription('\u200B')

            let index = 1

            for (const tournament of tournaments) {
                const {
                    emoji,
                    name,
                    startAt,
                    endAt,
                    city,
                    slug,
                    countryCode,
                    channelId,
                } = tournament

                embed.addFields({
                    name: `**${emoji}  __${name}__**`,
                    value: `\
**Date:** ${dateBuilder(startAt, endAt)}
**Lieu:** ${city} :flag_${countryCode.toLowerCase()}:
**Inscription:** https://www.start.gg/${slug}/details
**Discussion:** <#${channelId}>
${index < tournaments.length ? '‎' : ''}`,
                })
                index += 1
            }

            return [...acc, embed]
        },
        []
    )
}
