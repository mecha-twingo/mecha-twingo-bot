import moment from 'moment'

import { tournamentsEmbedBuilder } from '#helpers'

export default async (interaction, guild, client) => {
    if (
        !guild ||
        !guild.features?.monthliesMessages?.activated ||
        !guild?.features?.monthliesMessages?.tournaments?.channelId ||
        !guild?.features?.monthliesMessages?.calendar?.channelId
    ) {
        return
    }

    const operator = client ? client : interaction.client

    const monthSortedTournaments = guild.tournaments.reduce(
        (acc, cur) => ({
            ...acc,
            [moment(cur.startAt).locale('en').format('MMMM')]: [
                ...(acc[moment(cur.startAt).locale('en').format('MMMM')] ?? []),
                cur,
            ],
        }),
        {}
    )

    const channel = operator.channels.cache.get(
        guild?.features?.monthliesMessages?.calendar?.channelId
    )

    const embedMessage = [...(await channel.messages.fetch({ limit: 100 }))]
        .map(([, mess]) => mess)
        .find((mess) => mess.author.id === operator.user.id)

    const embeds = await tournamentsEmbedBuilder(monthSortedTournaments)

    if (!embeds.length) {
        if (embedMessage) {
            embedMessage.delete()
        }
        return
    }
    
    if (!embedMessage) {
        channel.send({ embeds })
    } else {
        embedMessage.edit({ embeds })
    }
}
