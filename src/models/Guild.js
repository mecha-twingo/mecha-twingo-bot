import mongoose, { Schema } from 'mongoose'
import { User, Tournament } from './index.js'

export default mongoose.model(
    'Guild',
    new Schema({
        id: String,
        googleSheetId: String,
        roles: {
            admin: String,
            weeklies: String,
            to: String,
        },
        features: {
            shutUp: {
                activated: {
                    type: Boolean,
                    default: false,
                    required: true,
                },
                cooldownedUsers: {
                    type: [String],
                    default: true,
                },
            },
            autoFeur: {
                activated: {
                    type: Boolean,
                    default: false,
                    required: true,
                },
            },
            weekliesMessages: {
                activated: { type: Boolean, default: false, required: true },
                agenda: {
                    channelId: String,
                    day: String,
                    hour: String,
                    roleId: String,
                },
                staff: {
                    day: String,
                    hour: String,
                    channelId: String,
                    roleId: String,
                },
            },
            monthliesMessages: {
                activated: { type: Boolean, default: false, required: true },
                calendar: {
                    channelId: String,
                },
                tournaments: {
                    channelId: String,
                },
            },
            weekliesGeneration: {
                activated: { type: Boolean, default: false, required: true },
                startgg: {
                    login: String,
                    password: String,
                },
                generation: {
                    day: String,
                    hour: String,
                },
            },
        },
        users: [User],
        tournaments: [Tournament],
    })
)
