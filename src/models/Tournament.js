import mongoose from 'mongoose'

export default new mongoose.Schema({
    id: String,
    emoji: String,
    name: String,
    startAt: Date,
    endAt: Date,
    city: String,
    slug: String,
    countryCode: String,
    channelId: String,
})
