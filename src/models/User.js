import mongoose from 'mongoose'

import config from '#config'
const { CHARACTERS } = config

export default new mongoose.Schema({
    id: String,
    skins: [
        {
            character: {
                type: String,
                enum: CHARACTERS,
            },
            skin: {
                type: Number,
                min: 1,
                max: 8,
            },
        },
    ],
})
