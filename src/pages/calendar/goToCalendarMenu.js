import { ButtonBuilder, ButtonStyle, ActionRowBuilder } from 'discord.js'

import { guildService } from '#services'

import { commandsHandler, menuPage, isActivated, isTwingo } from '#helpers'

import {
    goToCalendarMenu,
    goToThemesMenu,
    goToChannelDateHourRoleSelection,
} from '../index.js'

export default async (interaction, props = {}) => {
    const guild = await guildService.getOne(interaction.guildId)

    const calendarChannel = interaction.guild.channels.cache.find(
        (c) => c.id === guild.features.monthliesMessages.calendar.channelId
    )
    const tournamentsChannel = interaction.guild.channels.cache.find(
        (c) => c.id === guild.features.monthliesMessages.tournaments.channelId
    )

    const canActivate =
        calendarChannel && tournamentsChannel && isTwingo(interaction)

    const detailsContent =
        calendarChannel?.name || tournamentsChannel?.name
            ? `Canal pour le calendrier: \`${calendarChannel ? calendarChannel?.name : '🚫'}\`\n` +
              `Forums pour les tournois: \`${tournamentsChannel ? tournamentsChannel?.name : '🚫'}\`\n`
            : `**Aucune configuration**\n`

    await menuPage({
        description: "Choisis **l'option** que tu souhaites configurer",
        details:
            `__Calendrier des monthlies :__\n` +
            detailsContent +
            `\nL'annonce automatique de weeklies est actuellement **${isActivated(guild, 'monthliesMessages') ? 'Activé 🔊' : 'Désactivé 🔇'}**\n`,
        components: [
            new ActionRowBuilder()
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('activated')
                        .setLabel(
                            isActivated(guild, 'monthliesMessages')
                                ? 'Désactiver'
                                : 'Activer'
                        )
                        .setStyle(
                            isActivated(guild, 'monthliesMessages')
                                ? ButtonStyle.Danger
                                : ButtonStyle.Success
                        )
                        .setDisabled(!canActivate)
                )
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('calendar')
                        .setLabel('Canal pour calendrier 📣')
                        .setDisabled(
                            isActivated(guild, 'monthliesMessages') ||
                                !isTwingo(interaction)
                        )
                        .setStyle(ButtonStyle.Primary)
                )
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('tournament')
                        .setLabel('Forum pour tournois 📑')
                        .setDisabled(
                            isActivated(guild, 'monthliesMessages') ||
                                !isTwingo(interaction)
                        )
                        .setStyle(ButtonStyle.Primary)
                )
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('back')
                        .setLabel('Retour ↩️')
                        .setStyle(ButtonStyle.Secondary)
                ),
        ],
        handle: async (action) => {
            switch (action.customId) {
                case 'activated': {
                    if (!isActivated(guild, 'monthliesMessages')) {
                        commandsHandler.enableCommands(
                            [
                                'add_tournament_message',
                                'delete_tournament_message',
                                'refresh_tournament_messages'
                            ],
                            guild.id
                        )
                    } else {
                        commandsHandler.disableCommands(
                            [
                                'add_tournament_message',
                                'delete_tournament_message',
                                'refresh_tournament_messages'
                            ],
                            guild.id
                        )
                    }

                    const updatedGuild = await guildService.updateOne(
                        interaction.guildId,
                        {
                            'features.monthliesMessages.activated':
                                !isActivated(guild, 'monthliesMessages'),
                        }
                    )
                    await goToCalendarMenu(interaction, {
                        previousMessage: `Commandes \`add_tournament_message\`, \`refresh_tournament_messages\` & \`delete_tournament_message\` ${
                            isActivated(updatedGuild, 'monthliesMessages')
                                ? 'activée'
                                : 'désactivée'
                        } avec succès ✅`,
                    })
                    break
                }
                case 'calendar':
                    await goToChannelDateHourRoleSelection(interaction, {
                        initialMenu: goToCalendarMenu,
                        description: `Choisis le canal pour **le calendrier des monthlies**`,
                        paramToUpdate: 'features.monthliesMessages.calendar',
                        structure: {
                            channel: {
                                value: calendarChannel?.id,
                                type: 'annoucement',
                            },
                        },
                    })
                    break
                case 'tournament':
                    await goToChannelDateHourRoleSelection(interaction, {
                        initialMenu: goToCalendarMenu,
                        description: `Choisis le forum pour **le forum des tournois**`,
                        paramToUpdate: 'features.monthliesMessages.tournaments',
                        structure: {
                            channel: {
                                value: tournamentsChannel?.id,
                                type: 'forum',
                            },
                        },
                    })
                    break
                case 'back':
                    await goToThemesMenu(interaction)
                    break
            }
        },
        interaction,
        props,
    })
}
