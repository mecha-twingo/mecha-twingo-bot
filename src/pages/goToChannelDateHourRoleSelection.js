import config from '#config'
const { DAYS } = config

import {
    ButtonBuilder,
    ButtonStyle,
    ActionRowBuilder,
    ChannelSelectMenuBuilder,
    StringSelectMenuBuilder,
    StringSelectMenuOptionBuilder,
    RoleSelectMenuBuilder,
} from 'discord.js'

import { goToChannelDateHourRoleSelection } from './index.js'

import { guildService } from '#services'
import { menuPage } from '#helpers'

const hours = () => {
    let res = []
    for (let i = 0; i < 24; i++)
        res = [...res, { k: i.toString(), v: `${i < 10 ? '0' : ''}${i}:00` }]
    return res
}

export default async (interaction, props = {}) => {
    const { structure, initialMenu, description, paramToUpdate } = props

    for (const key of Object.keys(structure)) {
        structure[key] = { ...structure[key], activated: true }
    }

    const { channel, day, hour, role } = structure

    const components = []
    let type

    switch (channel?.type) {
        case 'forum':
            type = 15
            break
        case 'annoucement':
            type = 5
            break
        default:
            type = 0
    }

    if (structure.role?.activated) {
        components.push(
            new ActionRowBuilder().addComponents(
                new RoleSelectMenuBuilder()
                    .setCustomId('role')
                    .setPlaceholder('Choisis un role à mentionner')
                    .setDefaultRoles(role.value ?? 0)
            )
        )
    }

    if (structure.channel?.activated) {
        components.push(
            new ActionRowBuilder().addComponents(
                new ChannelSelectMenuBuilder()
                    .setCustomId('channel')
                    .setPlaceholder('Choisis un canal')
                    .setChannelTypes(type)
                    .setDefaultChannels(channel.value ?? 0)
            )
        )
    }

    if (structure.day?.activated) {
        components.push(
            new ActionRowBuilder().addComponents(
                new StringSelectMenuBuilder()
                    .setCustomId('day')
                    .setPlaceholder('Choisis un jour')
                    .addOptions(
                        Object.entries(DAYS).map(([k, v]) =>
                            new StringSelectMenuOptionBuilder()
                                .setLabel(v)
                                .setValue(k)
                                .setDefault(day.value === k)
                        )
                    )
            )
        )
    }

    if (structure.hour?.activated) {
        components.push(
            new ActionRowBuilder().addComponents(
                new StringSelectMenuBuilder()
                    .setCustomId('hour')
                    .setPlaceholder('Choisis un horaire')
                    .addOptions(
                        hours().map(({ k, v }) => {
                            return new StringSelectMenuOptionBuilder()
                                .setLabel(v)
                                .setValue(k)
                                .setDefault(hour.value === k)
                        })
                    )
            )
        )
    }

    menuPage({
        description,
        components: [
            ...components,
            new ActionRowBuilder()
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('validate')
                        .setLabel('Valider ✅')
                        .setDisabled(
                            !!(role?.activated && !role?.value) ||
                                !!(channel?.activated && !channel?.value) ||
                                !!(day?.activated && !day?.value) ||
                                !!(hour?.activated && !hour?.value)
                        )
                        .setStyle(ButtonStyle.Primary)
                )
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('back')
                        .setLabel('Retour ↩️')
                        .setStyle(ButtonStyle.Secondary)
                ),
        ],
        handle: async (action) => {
            switch (action.customId) {
                case 'validate':
                    await guildService.updateOne(interaction.guildId, {
                        ...(role?.activated
                            ? { [`${paramToUpdate}.roleId`]: role.value }
                            : {}),
                        ...(channel?.activated
                            ? { [`${paramToUpdate}.channelId`]: channel.value }
                            : {}),
                        ...(day?.activated
                            ? { [`${paramToUpdate}.day`]: day.value }
                            : {}),
                        ...(hour?.activated
                            ? { [`${paramToUpdate}.hour`]: hour.value }
                            : {}),
                    })
                    await initialMenu(interaction, {
                        previousMessage:
                            'Informations misent à jour avec succès ✅',
                    })
                    break
                case 'back':
                    await initialMenu(interaction)
                    break
                default:
                    structure[action.customId].value = action.values[0]
                    await goToChannelDateHourRoleSelection(interaction, props)
            }
        },
        interaction,
        props,
    })
}
