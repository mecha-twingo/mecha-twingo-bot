import { ButtonBuilder, ButtonStyle, ActionRowBuilder } from 'discord.js'

import {
    goToMiscsMenu,
    goToWeekliesMenu,
    goToThemesMenu,
    goToCalendarMenu
} from './index.js'

import { menuPage } from '#helpers'

export default async (interaction, props = {}) => {
    const { isLeaving } = props

    if (isLeaving) {
        setTimeout(async () => await interaction.deleteReply(), 1500)
        await menuPage({
            description: 'Bye bye 🤗',
            components: [],
            interaction,
        })
    } else {
        await menuPage({
            description:
                'Choisis le **thème** de la fonctionnalité que tu souhaites configurer',
            components: [
                new ActionRowBuilder()
                    .addComponents(
                        new ButtonBuilder()
                            .setCustomId('weeklies')
                            .setLabel('Weeklies 🍻')
                            .setStyle(ButtonStyle.Primary)
                    )
                    .addComponents(
                        new ButtonBuilder()
                            .setCustomId('calendar')
                            .setLabel('Calendrier 📅')
                            .setStyle(ButtonStyle.Primary)
                    )
                    .addComponents(
                        new ButtonBuilder()
                            .setCustomId('miscs')
                            .setLabel('Divers ⭐')
                            .setStyle(ButtonStyle.Primary)
                    )
                    .addComponents(
                        new ButtonBuilder()
                            .setCustomId('leave')
                            .setLabel('Quitter 🚪')
                            .setStyle(ButtonStyle.Secondary)
                    ),
            ],
            handle: async (action) => {
                switch (action.customId) {
                    case 'weeklies':
                        await goToWeekliesMenu(interaction)
                        break
                    case 'calendar':
                        await goToCalendarMenu(interaction)
                        break
                    case 'miscs':
                        await goToMiscsMenu(interaction)
                        break
                    case 'leave':
                        await goToThemesMenu(interaction, { isLeaving: true })
                        break
                }
            },
            interaction,
            props,
        })
    }
}
