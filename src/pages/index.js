import goToMiscsMenu from './miscs/goToMiscsMenu.js'
import goToWeekliesMenu from './weeklies/goToWeekliesMenu.js'
import goToThemesMenu from './goToThemesMenu.js'
import goToGenerationMenu from './weeklies/generation/goToGenerationMenu.js';
import goToAnnoucementMenu from './weeklies/annoucement/goToAnnoucementMenu.js'
import goToChannelDateHourRoleSelection from './goToChannelDateHourRoleSelection.js'
import goToCalendarMenu from './calendar/goToCalendarMenu.js'
import goToSettingsMenu from './weeklies/settings/goToSettingsMenu.js';

export {
    goToMiscsMenu,
    goToWeekliesMenu,
    goToThemesMenu,
    goToAnnoucementMenu,
    goToChannelDateHourRoleSelection,
    goToCalendarMenu,
    goToGenerationMenu,
    goToSettingsMenu
}
