import { ButtonBuilder, ButtonStyle, ActionRowBuilder } from 'discord.js'

import { guildService } from '#services'

import { commandsHandler, isActivated, menuPage, isTwingo } from '#helpers'

import { goToThemesMenu, goToMiscsMenu } from '../index.js'

export default async (interaction, props = {}) => {
    const guild = await guildService.getOne(interaction.guildId)

    await menuPage({
        description:
            'Choisis la **fonctionnalité** que tu souhaites configurer',
        components: [
            new ActionRowBuilder()
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('shutup')
                        .setLabel('/tg 🤬')
                        .setStyle(
                            isActivated(guild, 'shutUp')
                                ? ButtonStyle.Success
                                : ButtonStyle.Danger
                        )
                        .setDisabled(!isTwingo(interaction))
                )
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('autofeur')
                        .setLabel('Auto "FEUR" 💇')
                        .setStyle(
                            isActivated(guild, 'autoFeur')
                                ? ButtonStyle.Success
                                : ButtonStyle.Danger
                        )
                        .setDisabled(!isTwingo(interaction))
                )
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('back')
                        .setLabel('Retour ↩️')
                        .setStyle(ButtonStyle.Secondary)
                ),
        ],
        handle: async (action) => {
            switch (action.customId) {
                case 'shutup': {
                    if (!isActivated(guild, 'shutUp')) {
                        await commandsHandler.enableCommands(['tg'], guild.id)
                    } else {
                        await commandsHandler.disableCommands(['tg'], guild.id)
                    }
                    const updatedGuild = await guildService.updateOne(
                        interaction.guildId,
                        {
                            'features.shutUp.activated': !isActivated(
                                guild,
                                'shutUp'
                            ),
                        }
                    )
                    await goToMiscsMenu(interaction, {
                        previousMessage: `Commande \`/tg\` ${
                            isActivated(updatedGuild, 'shutUp')
                                ? 'activée'
                                : 'désactivée'
                        } avec succès ✅`,
                    })
                    break
                }
                case 'autofeur': {
                    const updatedGuild = await guildService.updateOne(
                        interaction.guildId,
                        {
                            'features.autoFeur.activated': !isActivated(
                                guild,
                                'autoFeur'
                            ),
                        }
                    )
                    await goToMiscsMenu(interaction, {
                        previousMessage: `Auto-FEUR ${
                            isActivated(updatedGuild, 'autoFeur')
                                ? 'activé'
                                : 'désactivé'
                        } avec succès ✅`,
                    })
                    break
                }
                case 'back':
                    await goToThemesMenu(interaction)
                    break
            }
        },
        interaction,
        props,
    })
}
