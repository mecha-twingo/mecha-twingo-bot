import config from '#config'
const { DAYS } = config

import { isActivated } from '#helpers'

export default (agendaRole, agendaChannel, staffRole, staffChannel, guild) => {
    const { agenda, staff } = guild.features.weekliesMessages

    const kindMessage = (kind, role, channel) => {
        if (Object.values(kind).some((v) => v !== undefined)) {
            return (
                `Role : \`${role?.name || '🚫'}\`\n` +
                `Canal : \`${channel?.name || '🚫'}\`\n` +
                `Jour : \`${kind?.day ? DAYS[kind.day] : '🚫'}\`\n` +
                `Heure : \`${kind?.hour ? `${kind.hour}:00` : '🚫'}\`\n`
            )
        }
        return `**Aucune configuration**\n`
    }

    return (
        `__Annonce pour les joueurs :__\n` +
        kindMessage(agenda, agendaRole, agendaChannel) +
        `\n__Annonce pour le staff :__\n` +
        kindMessage(staff, staffRole, staffChannel) +
        `\nL'annonce automatique de weeklies est actuellement **${isActivated(guild, 'weekliesMessages') ? 'Activé 🔊' : 'Désactivé 🔇'}**\n`
    )
}
