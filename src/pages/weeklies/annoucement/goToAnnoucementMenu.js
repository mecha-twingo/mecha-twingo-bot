import { ButtonBuilder, ButtonStyle, ActionRowBuilder } from 'discord.js'

import { guildService } from '#services'
import { cronHandler, menuPage, isActivated, isTwingo } from '#helpers'

import detailsBuilder from './detailsBuilder.js'

import {
    goToWeekliesMenu,
    goToAnnoucementMenu,
    goToChannelDateHourRoleSelection,
} from '../../index.js'

export default async (interaction, props = {}) => {
    const guild = await guildService.getOne(interaction.guildId)

    const { agenda, staff } = guild.features.weekliesMessages

    const agendaRole = interaction.guild.roles.cache.find(
        (r) => r.id === guild.features.weekliesMessages.agenda?.roleId
    )
    const agendaChannel = interaction.guild.channels.cache.find(
        (c) => c.id === guild.features.weekliesMessages.agenda?.channelId
    )
    const staffRole = interaction.guild.roles.cache.find(
        (r) => r.id === guild.features.weekliesMessages.staff?.roleId
    )
    const staffChannel = interaction.guild.channels.cache.find(
        (c) => c.id === guild.features.weekliesMessages.staff?.channelId
    )

    const canActivate =
        agendaRole &&
        agendaChannel &&
        staffRole &&
        staffChannel &&
        guild.features.weekliesMessages.staff?.day &&
        guild.features.weekliesMessages.staff?.hour &&
        guild.features.weekliesMessages.agenda?.day &&
        guild.features.weekliesMessages.agenda?.hour &&
        isTwingo(interaction)

    await menuPage({
        description: "Choisis **l'option** que tu souhaites configurer",
        details: detailsBuilder(
            agendaRole,
            agendaChannel,
            staffRole,
            staffChannel,
            guild
        ),
        components: [
            new ActionRowBuilder()
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('activated')
                        .setLabel(
                            isActivated(guild, 'weekliesMessages')
                                ? 'Désactiver'
                                : 'Activer'
                        )
                        .setStyle(
                            isActivated(guild, 'weekliesMessages')
                                ? ButtonStyle.Danger
                                : ButtonStyle.Success
                        )
                        .setDisabled(!canActivate)
                )
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('agenda')
                        .setLabel(`Agenda pour joueurs 👱🏼‍♂️`)
                        .setDisabled(
                            isActivated(guild, 'weekliesMessages') ||
                                !isTwingo(interaction)
                        )
                        .setStyle(ButtonStyle.Primary)
                )
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('staff')
                        .setLabel(`Agenda pour staff 👷‍♂️`)
                        .setDisabled(
                            isActivated(guild, 'weekliesMessages') ||
                                !isTwingo(interaction)
                        )
                        .setStyle(ButtonStyle.Primary)
                )
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('back')
                        .setLabel('Retour ↩️')
                        .setStyle(ButtonStyle.Secondary)
                ),
        ],
        handle: async (action) => {
            switch (action.customId) {
                case 'activated': {
                    const updatedGuild = await guildService.updateOne(
                        interaction.guildId,
                        {
                            'features.weekliesMessages.activated': !isActivated(
                                guild,
                                'weekliesMessages'
                            ),
                        }
                    )
                    await cronHandler.updateGuildCrons(
                        interaction.client,
                        updatedGuild
                    )
                    await goToAnnoucementMenu(interaction, {
                        previousMessage: `Annonce des weeklies ${isActivated(updatedGuild, 'weekliesMessages') ? 'activé' : 'désactivé'} avec succès ✅`,
                    })
                    break
                }
                case 'agenda':
                    await goToChannelDateHourRoleSelection(interaction, {
                        initialMenu: goToAnnoucementMenu,
                        description: `Choisis le canal, le jour et l'horaire pour l'annonce aux **joueurs** de la disponibilités **des weeklies**`,
                        paramToUpdate: 'features.weekliesMessages.agenda',
                        structure: {
                            role: { value: agendaRole?.id },
                            channel: {
                                value: agendaChannel?.id,
                                type: 'annoucement',
                            },
                            day: { value: agenda?.day },
                            hour: { value: agenda?.hour },
                        },
                    })
                    break

                case 'staff':
                    await goToChannelDateHourRoleSelection(interaction, {
                        initialMenu: goToAnnoucementMenu,
                        description: `Choisis le jour et l'horaire pour l'annonce aux **staffs** pour la configuration **des weeklies**.\n> \n> ⚠️ Le canal et le role se configurent dans le **menu principal** des weeklies. ⚠️`,
                        paramToUpdate: 'features.weekliesMessages.staff',
                        structure: {
                            day: { value: staff?.day },
                            hour: { value: staff?.hour },
                        },
                    })
                    break
                case 'back':
                    await goToWeekliesMenu(interaction)
                    break
            }
        },
        interaction,
        props,
    })
}
