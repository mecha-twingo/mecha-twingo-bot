import {
    ButtonBuilder,
    ButtonStyle,
    TextInputBuilder,
    ActionRowBuilder,
    ModalBuilder,
    TextInputStyle,
} from 'discord.js'

import { guildService } from '#services'
import { cronHandler, isActivated, menuPage, isTwingo } from '#helpers'

import {
    goToWeekliesMenu,
    goToGenerationMenu,
    goToChannelDateHourRoleSelection,
} from '../../index.js'

import config from '#config'
const { DAYS } = config

export default async (interaction, props = {}) => {
    const guild = await guildService.getOne(interaction.guildId)

    const staffChannel = interaction.guild.channels.cache.find(
        (c) => c.id === guild.features.weekliesMessages?.staff?.channelId
    )

    const { generation } = guild.features.weekliesGeneration

    const canActivate =
        generation?.day &&
        generation?.hour &&
        staffChannel &&
        isTwingo(interaction)

    const detailsContent =
        staffChannel || generation?.day || generation?.hour
            ? `Canal : \`${staffChannel ? staffChannel?.name : '🚫'}\`\n` +
              `Jour : \`${generation?.day ? DAYS[generation.day] : '🚫'}\`\n` +
              `Heure : \`${generation?.hour ? `${generation.hour}:00` : '🚫'}\`\n`
            : `**Aucune configuration**\n`

    await menuPage({
        description: "Choisis **l'option** que tu souhaites configurer",
        details:
            `__Génération des weeklies :__\n` +
            detailsContent +
            `\nLa génération automatique de weeklies est actuellement **${isActivated(guild, 'weekliesGeneration') ? 'Activé 🔊' : 'Désactivé 🔇'}**\n`,
        components: [
            new ActionRowBuilder()
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('activated')
                        .setLabel(
                            isActivated(guild, 'weekliesGeneration')
                                ? 'Désactiver'
                                : 'Activer'
                        )
                        .setStyle(
                            isActivated(guild, 'weekliesGeneration')
                                ? ButtonStyle.Danger
                                : ButtonStyle.Success
                        )
                        .setDisabled(!canActivate)
                )
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('generation')
                        .setLabel(`Génération ♻️`)
                        .setDisabled(
                            isActivated(guild, 'weekliesGeneration') ||
                                !isTwingo(interaction)
                        )
                        .setStyle(ButtonStyle.Primary)
                )
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('account')
                        .setLabel(`Compte start.gg 📝`)
                        .setDisabled(
                            isActivated(guild, 'weekliesGeneration') ||
                                !isTwingo(interaction)
                        )
                        .setStyle(ButtonStyle.Primary)
                )
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('back')
                        .setLabel('Retour ↩️')
                        .setStyle(ButtonStyle.Secondary)
                ),
        ],
        handle: async (action) => {
            switch (action.customId) {
                case 'activated': {
                    await action.deferUpdate()
                    const updatedGuild = await guildService.updateOne(
                        interaction.guildId,
                        {
                            'features.weekliesGeneration.activated':
                                !isActivated(guild, 'weekliesGeneration'),
                        }
                    )
                    await cronHandler.updateGuildCrons(
                        interaction.client,
                        updatedGuild
                    )
                    await goToGenerationMenu(interaction, {
                        previousMessage: `Génération des weeklies ${isActivated(updatedGuild, 'weekliesGeneration') ? 'activé' : 'désactivé'} avec succès ✅`,
                    })
                    break
                }
                case 'generation':
                    await action.deferUpdate()
                    await goToChannelDateHourRoleSelection(interaction, {
                        initialMenu: goToGenerationMenu,
                        description: `Choisis le canal, le jour et l'horaire pour la génération **des weeklies**\n> \n> ⚠️ Le canal se configure dans le **menu principal** des weeklies. ⚠️`,
                        paramToUpdate: 'features.weekliesGeneration.generation',
                        structure: {
                            day: { value: generation?.day },
                            hour: { value: generation?.hour },
                        },
                    })
                    break
                case 'account': {
                    const modal = new ModalBuilder()
                        .setCustomId('modal')
                        .setTitle('Compte start.gg')
                        .addComponents([
                            new ActionRowBuilder().addComponents(
                                new TextInputBuilder()
                                    .setCustomId('login')
                                    .setLabel('E-mail')
                                    .setStyle(TextInputStyle.Short)
                                    .setRequired(true)
                            ),
                            new ActionRowBuilder().addComponents(
                                new TextInputBuilder()
                                    .setCustomId('password')
                                    .setLabel('Mot de passe')
                                    .setStyle(TextInputStyle.Short)
                                    .setRequired(true)
                            ),
                        ])

                    await action.showModal(modal)

                    const modalReturn = await action.awaitModalSubmit({
                        time: 60000,
                    })

                    modalReturn.deferUpdate()

                    const login = modalReturn.fields.getTextInputValue('login')
                    const password =
                        modalReturn.fields.getTextInputValue('password')

                    await guildService.updateOne(interaction.guildId, {
                        'features.weekliesGeneration.startgg.login': login,
                        'features.weekliesGeneration.startgg.password':
                            password,
                    })

                    await goToGenerationMenu(interaction, {
                        previousMessage: '✅ Compte mit à jour avec succès',
                    })
                    break
                }
                case 'back':
                    await action.deferUpdate()
                    await goToWeekliesMenu(interaction)
                    break
            }
        },
        interaction,
        props,
        customDeferUpdate: true,
    })
}
