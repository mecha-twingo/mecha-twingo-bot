import {
    ButtonBuilder,
    ButtonStyle,
    ActionRowBuilder,
} from 'discord.js'

import {
    goToThemesMenu,
    goToAnnoucementMenu,
    goToGenerationMenu,
    goToSettingsMenu
} from '../index.js'

import { menuPage } from '#helpers'

export default async (interaction, props = {}) =>
    await menuPage({
        description:
            'Choisis la **fonctionnalité** que tu souhaites configurer',
        components: [
            new ActionRowBuilder()
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('annoucement')
                        .setLabel('Annonce 📣')
                        .setStyle(ButtonStyle.Primary)
                )

                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('generate')
                        .setLabel('Génération 🪩')
                        .setStyle(ButtonStyle.Primary)
                )

                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('settings')
                        .setLabel('Paramètres 🛠️')
                        .setStyle(ButtonStyle.Primary)
                )

                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('back')
                        .setLabel('Retour ↩️')
                        .setStyle(ButtonStyle.Secondary)
                ),
        ],
        handle: async (action) => {
            switch (action.customId) {
                case 'annoucement':
                    await goToAnnoucementMenu(interaction)
                    break
                case 'generate':
                    await goToGenerationMenu(interaction)
                    break
                case 'settings':
                    await goToSettingsMenu(interaction)
                    break
                case 'back':
                    await goToThemesMenu(interaction)
                    break
            }
        },
        interaction,
        props,
    })
