import {
    ButtonBuilder,
    ButtonStyle,
    TextInputBuilder,
    ActionRowBuilder,
    ModalBuilder,
    TextInputStyle,
} from 'discord.js'

import {
    goToSettingsMenu,
    goToWeekliesMenu,
    goToChannelDateHourRoleSelection,
} from '../../index.js'

import { guildService } from '#services'

import { isTwingo, menuPage } from '#helpers'

export default async (interaction, props = {}) => {
    const guild = await guildService.getOne(interaction.guildId)

    const staffRole = interaction.guild.roles.cache.find(
        (r) => r.id === guild.features.weekliesMessages?.staff?.roleId
    )
    const staffChannel = interaction.guild.channels.cache.find(
        (c) => c.id === guild.features.weekliesMessages?.staff?.channelId
    )

    return await menuPage({
        description:
            'Choisis la **fonctionnalité** que tu souhaites configurer',
        components: [
            new ActionRowBuilder()
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('staff')
                        .setLabel('Role & canal staff 👷‍♂️')
                        .setStyle(ButtonStyle.Primary)
                        .setDisabled(!isTwingo(interaction))
                )

                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('gsheet')
                        .setLabel('GoogleSheet 🗂️')
                        .setStyle(ButtonStyle.Primary)
                        .setDisabled(!isTwingo(interaction))
                )

                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('back')
                        .setLabel('Retour ↩️')
                        .setStyle(ButtonStyle.Secondary)
                ),
        ],
        handle: async (action) => {
            switch (action.customId) {
                case 'staff':
                    await action.deferUpdate()
                    await goToChannelDateHourRoleSelection(interaction, {
                        initialMenu: goToSettingsMenu,
                        description: `Choisis le rôle et le canal pour le staff`,
                        paramToUpdate: 'features.weekliesMessages.staff',
                        structure: {
                            role: { value: staffRole?.id },
                            channel: { value: staffChannel?.id },
                        },
                    })
                    break
                case 'gsheet': {
                    const modal = new ModalBuilder()
                        .setCustomId('modal')
                        .setTitle('Google Sheet')
                        .addComponents([
                            new ActionRowBuilder().addComponents(
                                new TextInputBuilder()
                                    .setCustomId('gsheetConfig')
                                    .setLabel(
                                        'ID du google sheet reférençant les weeklies'
                                    )
                                    .setStyle(TextInputStyle.Short)
                                    .setRequired(true)
                            ),
                        ])

                    await action.showModal(modal)

                    const modalReturn = await action.awaitModalSubmit({
                        time: 60000,
                    })
                    modalReturn.deferUpdate()

                    const googleSheetId =
                        modalReturn.fields.getTextInputValue('gsheetConfig')
                    await guildService.updateOne(interaction.guildId, {
                        googleSheetId,
                    })

                    await goToWeekliesMenu(interaction, {
                        previousMessage:
                            '✅ Googlesheet mit à jour avec succès',
                    })
                    break
                }
                case 'back':
                    await action.deferUpdate()
                    await goToWeekliesMenu(interaction)
                    break
            }
        },
        interaction,
        props,
        customDeferUpdate: true,
    })
}
