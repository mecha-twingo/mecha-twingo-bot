import { Guild } from '../models/index.js'

export const getOne = async (id) => await Guild.findOne({ id })

export const initialize = async (id) => await new Guild({ id }).save()

export const getAll = async () => await Guild.find()

export const updateOne = async (id, config) =>
    await Guild.findOneAndUpdate(
        { id: { $eq: id } },
        { $set: config },
        { new: true }
    )

export const getTournamentsBySlug = async (id, slug) => {
    const guild = await Guild.findOne({ id: { $eq: id } })
    return guild.tournaments.find((t) => t.slug === `tournament/${slug}`)
}

export const isChannelIdUsedOnce = async (id, channelId) => {
    const guild = await Guild.findOne({ id: { $eq: id } })
    return (
        guild.tournaments.filter((t) => t.channelId === channelId).length === 1
    )
}

export const insertTournament = async (id, tournament) => {
    const guild = await Guild.findOne({ id })

    if (guild.tournaments.some((t) => t.slug === tournament.slug)) {
        return guild
    }

    const tournaments = [...guild.tournaments, tournament].sort(
        (a, b) => new Date(a.startAt) - new Date(b.startAt)
    )
    return await Guild.findOneAndUpdate(
        { id },
        { $set: { tournaments } },
        { new: true }
    )
}

export const deleteTournament = async (id, slug) =>
    await Guild.findOneAndUpdate(
        { id: { $eq: id } },
        { $pull: { tournaments: { slug: { $eq: `tournament/${slug}` } } } },
        { new: true }
    )

export const deletePreviousTournaments = async (id) => {
    const now = new Date()
    const month = now.getMonth()
    const year = now.getFullYear()

    const lastMonth = month === 0 ? 11 : month - 1
    const lastMonthYear = month === 0 ? year - 1 : year

    const limitDate = new Date(lastMonthYear, lastMonth, 15)

    return await Guild.findOneAndUpdate(
        { id: { $eq: id } },
        { $pull: { tournaments: { startAt: { $lt: limitDate } } } },
        { new: true }
    )
}

export const resetAllCooldowns = async () => {
    await Guild.updateMany(
        {},
        { $set: { 'features.shutUp.cooldownedUsers': [] } }
    )
}

export const addUserInCooldown = async (id, userId) =>
    await Guild.updateOne(
        { id },
        { $push: { 'features.shutUp.cooldownedUsers': userId } }
    )
